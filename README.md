# BlockDuster ver 2.0

Python application 'Blockduster' is upgraded to ver 2.0
This upgrade will have Flask and pyOdbc to access database and will also use HTML/Java and CSS to show different web pages

## Reference

https://www.youtube.com/watch?v=Z1RJmh_OqeA

## Getting started

This is a Flask application as a sample/experiment to learn flask.
As a database it is using sql server
All dependencies will be installed locally using virtualenv within the project

## Setup for this app

- Install Python 3 [Make usre you do not have another version installed]
- Follow these commands below to setup the environment

Install virtual environment

```
pip3 install virtualenv
```

Once the environment is installed, we will initialize it to a variable 'env' is the variable we are using. Normally that is used but it can be anything
After it is setup, we will activate this environment so anything we install here will not be installed to the system globally. It will be confined to the boundries of this project

```
virtualenv env
source env/bin/activate
```

After the environment is setup and ready, we will install flask and flask-sqlalchemy

```
pip3 install flask flask-sqlalchemy
```

After this environment is all set, we will start the project and start writing our code

# Windows/Mac users

- NOTE: If you have python 2.7 installed on your windows environment, make sure all the paths are updated to verion 3.10 or higher.
  Make sure that %PATH% is pointing to version 3.10 or higher and also I would recommend removing python27 folder from the machine.
  Also, make sure that you point to python3 when running your code in visual studio or some other IDE.
  For VS code

```
goto View->Command Pallet
Type: Python: Select Interpreter
Select your python3
```

## Already installed or copy/paste this code

If you have environment folder already in the folder than you do not need to run 'virtualenv env'. Environment already exists so all what you can do is initialize that environment or point to that environment to run you web server. Follow the following commands:

- Windows users

```
env3/scripts/activate
```

- Mac Users

```
source env3/bin/activate
```

Once done with the site, close the environment

```
deactivate
```
