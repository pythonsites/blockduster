# Blockduster ver 1.0
import pyodbc as pyodbc
from flask import Flask, redirect, render_template, url_for, request

app = Flask('__name__')

connString = pyodbc.connect(
    "Driver={SQL Server Native Client 11.0};" 
    "Server=AMMAR-HP\MSSQLSERVER01;"
    "Database=blockduster;"
    "Username=blockduster;"
    "Password=bd1234!;"
    "Trusted_Connection=yes;"
)

def readRentedMovies(conn):
    cursor = conn.cursor()
    cursor.execute("""select Customers.Name AS Customer, Movies.Name AS Movie, ActiveRentals.RentedON, ActiveRentals.DueBY 
                        from ActiveRentals 
                        left Outer Join Customers
                          on ActiveRentals.Customer_ID = Customers.ID
                        left Outer Join Movies
                          on ActiveRentals.Movie_ID = Movies.ID
                    """)
    return cursor

def readAllMovies(conn):
    cursor = conn.cursor()
    cursor.execute("select * from movies")
    return cursor

def readOneMovie(conn, ID):
    cursor = conn.cursor()
    cursor.execute("select * from movies where ID = ?", (ID))
    return cursor    

@app.route('/Dashboard/', methods=['POST','GET'])
def Dashboard():
    # This is a GET method which means get the information and show it on the screen. Here we are showing everything
    # get al records in the database and show it on the screen
    allRecords = readAllMovies(connString)

    # data = {'Work' : '22,10', 'Eat' : '4,2', 'Commute' : '6,4', 'Watching TV' : '5,1', 'Sleeping' : '15,10'}
    data = {}

    # We are not adding titles row. It is already there
    for movie in allRecords:
        data[movie.Name] = str(movie.NoOfCopies) + "," + str(movie.NoOfRented)

    # Data for active rentals
    rentedData = readRentedMovies(connString)

    # under performing movies chart
    

    return render_template('Dashboard.html', data=data, Rentals=rentedData)

@app.route('/', methods=['POST','GET'])
def index():
    # This is a GET method which means get the information and show it on the screen. Here we are showing everything
    # get al records in the database and show it on the screen
    allRecords = readAllMovies(connString)
    return render_template('index.html', tasks=allRecords)

@app.route('/movies/', methods=['POST','GET'])
def movies():
    # This is a GET method which means get the information and show it on the screen. Here we are showing everything
    # get al records in the database and show it on the screen
    allRecords = readAllMovies(connString)
    return render_template('index.html', tasks=allRecords)

@app.route('/details/', methods=['GET'])
def details():
    # This is a GET method which means get the information and show it on the screen. Here we are showing everything
    # get all records in the database and show it on the screen
    allRecords = readAllMovies(connString)
    return render_template('details.html', tasks=allRecords)

@app.route('/edit/<id>/', methods=['POST','GET'])
def edit(id):
    if request.method == 'POST':
        nameChanged = request.form['name']
        totalChanged = request.form['total']
        rentedChanged = request.form['rented']
        sqlStatement = "UPDATE movies SET Name = ?, NoOfCopies = ?, NoOfRented = ? where ID = %s" % id
        statementVal = (nameChanged, totalChanged, rentedChanged)
        cursor = connString.cursor()
        cursor.execute(sqlStatement, statementVal)
        cursor.commit()
        return redirect('/')
    else:
        selectedRecord = readOneMovie(connString, id)
        return render_template('editAMovie.html', selectedMovie=selectedRecord)

@app.route('/delete/<id>/', methods=['POST','GET'])
def delete(id):
    # This is a GET method which means get the information and show it on the screen. Here we are showing everything
    # get al records in the database and show it on the screen
    allRecords = readAllMovies(connString)
    return render_template('details.html', tasks=allRecords)

# Main entrypoint of this website
if __name__ == "__main__":
    app.run(host="localhost",port=3000,debug=True)
